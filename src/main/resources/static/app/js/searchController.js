mainApp.controller("searchController", function($scope, $location, $http, $routeParams){
	
	const searchBooksUrl = "/api/books";
	
	$scope.booksSearch = [];

	$scope.searchParams = {};
	$scope.searchParams.author = "";
	$scope.searchParams.language = "";
	$scope.searchParams.title = "";
	$scope.searchParams.year = "";

	$scope.page = 0;
	$scope.totalPages = 0;
	
//   ------------------------------------------------------------------------------------
//     
//   ------------------------------------------------------------------------------------

	var getBooksSearch = function(){
		var config = {params: {}};
		config.params.page = $scope.page;

		if($scope.searchParams.author != ""){
			config.params.author = $scope.searchParams.author;
		}
		if($scope.searchParams.language != ""){
			config.params.language = $scope.searchParams.language;
		}
		if($scope.searchParams.title != ""){
			config.params.title = $scope.searchParams.title;
		}
		if($scope.searchParams.year != ""){
			config.params.year = $scope.searchParams.year;
		}
				
		config.params.page = $scope.page;

		$http.get(searchBooksUrl, config).then(
			function(response){
				$scope.booksSearch = response.data;
				$scope.totalPages = response.headers("totalPages");
			},
			function(){
				alert("Couldn't get books");
			}
		);
	}
			
			

		
	

// ----------------------------------------------------
// 			PRETRAGA I PAGINACIJA 
// ----------------------------------------------------

	$scope.doSearch = function(){
		$scope.page = 0;
		getBooksSearch();
	}

	$scope.changePage = function(direction){
		$scope.page = $scope.page + direction;
		getBooksSearch();
	}

});

