var mainApp = angular.module("mainApp", ["ngRoute"]);

mainApp.config(['$routeProvider', function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl : '/app/html/home.html',
			controller: 'homeController'
		})
		.when('/add', {
			templateUrl : '/app/html/add.html',
			controller: 'addController'
		})
		.when('/search', {
			templateUrl : '/app/html/search.html',
			controller: 'searchController'
		})
		.when('/books/edit/:bid', {
			templateUrl : '/app/html/edit.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);

mainApp.controller("homeController", function($scope, $location, $http, $routeParams){
	
	$scope.message = "Book Catalogue";

	const baseBooksUrl = "/api/books";


	$scope.page = 0;
	$scope.totalPages = 0;

	$scope.books =[];
	$scope.book = {};
	
	$scope.saved = {};

	
//   ------------------------------------------------------------------------------------
//     
//   ------------------------------------------------------------------------------------

	let getBooks = function(){
		let config = {params: {}};
		config.params.page = $scope.page;

		$http.get(baseBooksUrl, config).then(
			function(response){
				$scope.books = response.data;
				$scope.totalPages = response.headers("totalPages");
			},
			function(){
				alert("Couldn't get books");
			}
		);
	}

	getBooks();
				
	$scope.getBook = function(id){
		let getBookUrl = baseBooksUrl + "/" + id;
		$http.get(getBookUrl).then(
			function(response){
				$scope.book = response.data;
			},
			function(){
				alert("Couldn't get book");
			}
		);
	}

	$scope.remove = function(id){
		let deleteBookUrl = baseBooksUrl + "/" + id;
		$http.delete(deleteBookUrl).then(
			function success(){
				getBooks();
			},
			function error(){
				alert("Couldn't delete book");
			}
		);
	}

	
	$scope.goToEdit = function(id){
		$location.path("/books/edit/" + id);
	}
	

// ----------------------------------------------------
// 			PAGINACIJA 
// ----------------------------------------------------

	$scope.changePage = function(direction){
		$scope.page = $scope.page + direction;
		getBooks();
	}

});

