mainApp.controller("editController", function($scope, $location, $http, $routeParams){
	
	const editUrl = "/api/books/" + $routeParams.bid;
	
	$scope.edited = {};
				
	const getBook = function(){
		$http.get(editUrl).then(
			function(response){
				$scope.edited = response.data;
			},
			function(){
				alert("Couldn't get book");
			}
		);
	}
	
	getBook();

	$scope.save = function(){
		$http.put(editUrl, $scope.edited).then(
			function success(response){
				$scope.edited = {};
				alert("Book edited!");
				$location.path("/");
			},
			function error(){
				alert("Couldn't edit book");
			}
		);
	}

	
});

