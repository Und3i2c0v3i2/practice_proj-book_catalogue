mainApp.controller("addController", function($scope, $location, $http){

	const urlAdd = "/api/books/";
	$scope.saved = {};
	
	$scope.save = function(){
		$http.post(urlAdd, $scope.saved).then(
			function success(){
				alert("Book saved!");
				$location.path("/");
			},
			function error(){
				alert("Couldn't save a book");
			}
		);
	}

});
