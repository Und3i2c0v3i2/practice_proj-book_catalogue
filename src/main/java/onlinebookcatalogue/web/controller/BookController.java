package onlinebookcatalogue.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import onlinebookcatalogue.model.Book;
import onlinebookcatalogue.service.BookService;
import onlinebookcatalogue.web.dto.BookDTO;


@RestController
@RequestMapping(value="api/books")
public class BookController {
	
	@Autowired
	private BookService bookService;
	
	private ModelMapper modelMapper = new ModelMapper();

	
	@GetMapping
	public ResponseEntity<List<BookDTO>> getAll(
			@RequestParam("author") Optional<String> author,
			@RequestParam("language") Optional<String> language,
			@RequestParam("title") Optional<String> title,
			@RequestParam("year") Optional<Integer> year,
			@RequestParam(value="page", defaultValue="0") int page) {
		
		Page<Book> books = null;

		if(author.isPresent() || language.isPresent() || title.isPresent() || year.isPresent()) {
			books = bookService.search(author, language, title, year, page);
		} else {
			books = bookService.getEntireCatalogue(page);
		}
		
		List<BookDTO> dtos = new ArrayList<BookDTO>();
		for(Book b : books.getContent()) {
			dtos.add(modelMapper.map(b, BookDTO.class));
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(books.getTotalPages()) );
		
		return new ResponseEntity<>(dtos, headers, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<BookDTO> getOne(
			@PathVariable Long id) {
		
		Book book = bookService.getBookById(id);
		
		return new ResponseEntity<>(modelMapper.map(book, BookDTO.class), HttpStatus.OK);
	}

	
	@PostMapping
	public ResponseEntity<Void> save(@RequestBody BookDTO book) {
		
		if(book.getId() != null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			bookService.registerNewBook(modelMapper.map(book, Book.class));
			return new ResponseEntity<>(HttpStatus.CREATED);
		}

	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Void> edit(
			@RequestBody BookDTO book,
			@PathVariable Long id) {
		
		if(id == book.getId()) {
			bookService.registerNewBook(modelMapper.map(book, Book.class));
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		
		bookService.removeBookById(id);

		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
