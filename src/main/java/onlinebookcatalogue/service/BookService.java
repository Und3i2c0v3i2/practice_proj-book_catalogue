package onlinebookcatalogue.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import onlinebookcatalogue.model.Book;

public interface BookService {

	// CRUD
	public void registerNewBook(Book book);
	public void registerNewBooks(List<Book> books);
	public Page<Book> getEntireCatalogue(int page);
	public Book getBookById(Long id);
	public void removeBookById(Long id);
	
	// SEARCH
	public Page<Book> search(@Param("author") Optional<String> author,
							 @Param("language") Optional<String> language,
							 @Param("title") Optional<String> title,
							 @Param("year") Optional<Integer> year,
							 int page);
		
	public List<Book> findBookByAuthor(String author);
	public List<Book> findBookByLanguage(String language);
	public List<Book> findBookByTitle(String title);
	public List<Book> findBookByYear(int year);
}
