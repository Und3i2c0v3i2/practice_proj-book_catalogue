package onlinebookcatalogue.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import onlinebookcatalogue.model.Book;
import onlinebookcatalogue.repository.BookRepository;
import onlinebookcatalogue.service.BookService;


@Service
public class BookServiceImpl 
	implements BookService {

	@Autowired
	private BookRepository bookRepository;

	@Override
	public void registerNewBook(Book book) {
		bookRepository.save(book);
		
	}
	
	@Override
	public void registerNewBooks(List<Book> books) {
		
		books.stream()
			.forEach(b -> bookRepository.save(b));
		
//		for(Book b : books) {
//			bookRepository.save(b);
//		}
		
	}

	@Override
	public Page<Book> getEntireCatalogue(int page) {
		return bookRepository.findAll(PageRequest.of(page, 5));
	}

	@Override
	public Book getBookById(Long id) {
		return bookRepository.findById(id)
							 .orElseThrow(() -> new EntityNotFoundException("Couldn't find book with id " + id));
	}
	
	@Override
	public void removeBookById(Long id) {
		bookRepository.deleteById(id);
		
	}

	@Override
	public List<Book> findBookByAuthor(String author) {
		return bookRepository.findByAuthor(author);
	}

	@Override
	public List<Book> findBookByLanguage(String language) {
		return bookRepository.findByLanguage(language);
	}


	@Override
	public List<Book> findBookByTitle(String title) {
		return bookRepository.findByTitle(title);
	}

	@Override
	public List<Book> findBookByYear(int year) {
		return bookRepository.findByYear(year);
	}

	@Override
	public Page<Book> search(Optional<String> author, 
							 Optional<String> language, 
							 Optional<String> title,
							 Optional<Integer> year, 
							 int page) {
		
		return bookRepository.search(author, language, title, year, PageRequest.of(page, 5));

	}
	

}
