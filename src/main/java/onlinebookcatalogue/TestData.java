package onlinebookcatalogue;


import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import onlinebookcatalogue.model.Book;
import onlinebookcatalogue.service.BookService;


@Component
public class TestData {
		
	@Autowired
	BookService bookService;
	
	@PostConstruct
	public void init() {
		
		// read json and write to db
		
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<Book>> typeReference = new TypeReference<List<Book>>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream("/mock-data/data.json");
		try {
			List<Book> books = mapper.readValue(inputStream,typeReference);
			bookService.registerNewBooks(books);
			System.out.println("Books Saved!");
		} catch (IOException e){
			System.out.println("Unable to save books: " + e.getMessage());
		}

	}


}
