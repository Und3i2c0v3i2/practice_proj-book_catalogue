package onlinebookcatalogue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class App {

	public static void main(String[] args) {
		
		SpringApplication.run(App.class, args);

	}

//  Read data from Json
//	
//	@Bean
//	CommandLineRunner runner(BookService bookService) {
//		return args -> {
//			// read json and write to db
//			ObjectMapper mapper = new ObjectMapper();
//			TypeReference<List<Book>> typeReference = new TypeReference<List<Book>>(){};
//			InputStream inputStream = TypeReference.class.getResourceAsStream("/mock-data/data.json");
//			try {
//				List<Book> books = mapper.readValue(inputStream,typeReference);
//				bookService.registerNewBooks(books);
//				System.out.println("Books Saved!");
//			} catch (IOException e){
//				System.out.println("Unable to save books: " + e.getMessage());
//			}
//		};
//	}
	
}
