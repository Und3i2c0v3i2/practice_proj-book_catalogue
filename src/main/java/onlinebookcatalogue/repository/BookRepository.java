package onlinebookcatalogue.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import onlinebookcatalogue.model.Book;


@Repository
public interface BookRepository 
	extends JpaRepository<Book, Long>{
	
	List<Book> findByAuthor(String author);
	List<Book> findByLanguage(String language);
	List<Book> findByTitle(String title);
	List<Book> findByYear(int year);
	
	@Query("SELECT b FROM Book b WHERE "
			+ "(:author IS NULL OR b.author = :author ) AND "
			+ "(:language IS NULL OR b.language = :language) AND "
			+ "(:title IS NULL OR b.title = :title) AND "
			+ "(:year IS NULL OR b.year = :year ) "
			)
	Page<Book> search(@Param("author") Optional<String> author, 
					  @Param("language") Optional<String> language, 
					  @Param("title") Optional<String> title,
					  @Param("year") Optional<Integer> year,
					  Pageable pageRequest);
}
